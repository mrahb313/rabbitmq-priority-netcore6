﻿using RabbitMQPriority.Producer.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPriority.Producer.Dto
{
    [Serializable]
    public class OrderDto
    {
        public int id { get; set; }
        public int userId { get; set; }
        public ActionType actionType { get; set; }
        public string symbol { get; set; }
        public decimal amount { get; set; }
    }
}
