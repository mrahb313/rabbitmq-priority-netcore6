﻿using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using RabbitMQPriority.Producer.Dto;
using RabbitMQPriority.Producer.Enum;
using RabbitMQPriority.Producer.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPriority.Producer.Services
{
    public class ProduceService : BackgroundService
    {
        private readonly IRabbitManager _rabbitManager;

        public ProduceService(IRabbitManager rabbitManager)
        {
            _rabbitManager = rabbitManager;
        }
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            List<OrderDto> orderList = new()
            {
                new OrderDto { id = 1, userId = 1, actionType = ActionType.Create, symbol = "ADAUSDT", amount = 20 },
                new OrderDto { id = 2, userId = 1, actionType = ActionType.Create, symbol = "ADAUSDT", amount = 50 },
                new OrderDto { id = 3, userId = 1, actionType = ActionType.Cancel, symbol = "ADAUSDT", amount = 110 },
                new OrderDto { id = 4, userId = 1, actionType = ActionType.Create, symbol = "ADAUSDT", amount = 147 },
                new OrderDto { id = 5, userId = 1, actionType = ActionType.Cancel, symbol = "ADAUSDT", amount = 168 },
                new OrderDto { id = 6, userId = 1, actionType = ActionType.Create, symbol = "ADAUSDT", amount = 168 },
                new OrderDto { id = 7, userId = 1, actionType = ActionType.Cancel, symbol = "ADAUSDT", amount = 168 },
                new OrderDto { id = 8, userId = 1, actionType = ActionType.Create, symbol = "ADAUSDT", amount = 168 },
                new OrderDto { id = 9, userId = 1, actionType = ActionType.Create, symbol = "ADAUSDT", amount = 168 },
                new OrderDto { id = 10, userId = 1, actionType = ActionType.Cancel, symbol = "ADAUSDT", amount = 168 },
            };

            foreach (var order in orderList)
            {
                _rabbitManager.Publish(order);
            }

            return Task.CompletedTask;
        }
    }
}
