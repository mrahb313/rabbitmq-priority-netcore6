﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPriority.Producer.Enum
{
    public enum ActionType
    {
        Create,
        Cancel
    }
}
