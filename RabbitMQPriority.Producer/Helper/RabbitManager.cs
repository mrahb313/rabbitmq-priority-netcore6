﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQPriority.Producer.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPriority.Producer.Helper
{
    public interface IRabbitManager
    {
        void Publish(OrderDto dto);
    }
    public class RabbitManager : IRabbitManager
    {
        private readonly IConfiguration _configuration;
        private ConnectionFactory _factory = new ConnectionFactory();
        private IConnection _conn;
        private IModel _channelPublish;

        public RabbitManager(IConfiguration configuration)
        {
            _configuration = configuration;

            var username = _configuration.GetSection("Rabbit")["Username"];
            var password = _configuration.GetSection("Rabbit")["Password"];
            var host = _configuration.GetSection("Rabbit")["Host"];
            var ip = _configuration.GetSection("Rabbit")["Ip"];

            _factory.UserName = username;
            _factory.Password = password;
            _factory.VirtualHost = host;
            _factory.HostName = ip;
            _factory.AutomaticRecoveryEnabled = true;
            _factory.DispatchConsumersAsync = true;

            _conn = _factory.CreateConnection();

            _channelPublish = _conn.CreateModel();
            IDictionary<String, Object> args = new Dictionary<String, Object>();
            args.Add("x-max-priority", 2);
            _channelPublish.QueueDeclare(queue: "order", durable: false, exclusive: false, autoDelete: false, args);
        }
        public void Publish(OrderDto dto)
        {
            var jsonSerialized = JsonConvert.SerializeObject(dto);
            var body = Encoding.UTF8.GetBytes(jsonSerialized);

            var properties = _channelPublish.CreateBasicProperties();
            properties.Priority = Convert.ToByte(dto.actionType);

            _channelPublish.BasicPublish(exchange: "", routingKey: "order", basicProperties: properties, body: body);

        }
    }
}
