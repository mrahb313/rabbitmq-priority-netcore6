﻿
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RabbitMQPriority.Producer.Dto;
using RabbitMQPriority.Producer.Enum;
using RabbitMQPriority.Producer.Helper;
using RabbitMQPriority.Producer.Services;

namespace RabbitMQPriority.Producer
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Producer Executed");

            var currentPath = Directory.GetCurrentDirectory();
            var rootPath = Path.GetFullPath(Path.Combine(currentPath, @"..\..\..\"));

            IConfiguration Configuration = new ConfigurationBuilder()
           .SetBasePath(rootPath)
           .AddJsonFile("appsettings.json")
           .AddEnvironmentVariables()
           .Build();

            var hostBuilder = new HostBuilder()
              .ConfigureServices(services =>
              {
                  services.AddSingleton<IConfiguration>(Configuration);
                  services.AddSingleton<IRabbitManager, RabbitManager>();
                  services.AddHostedService<ProduceService>();
              })
              .UseConsoleLifetime()
              .Build();

            Console.WriteLine("Producer Is Running");
            hostBuilder.Run();

        }
    }
}
