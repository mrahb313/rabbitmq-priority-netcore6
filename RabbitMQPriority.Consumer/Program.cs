﻿
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RabbitMQPriority.Consumer.Services;

namespace RabbitMQPriority.Consumer
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Consumer Executed");

            var currentPath = Directory.GetCurrentDirectory();
            var rootPath = Path.GetFullPath(Path.Combine(currentPath, @"..\..\..\"));

            IConfiguration Configuration = new ConfigurationBuilder()
           .SetBasePath(rootPath)
           .AddJsonFile("appsettings.json")
           .AddEnvironmentVariables()
           .Build();

            var hostBuilder = new HostBuilder()
              .ConfigureServices(services =>
              {
                  services.AddSingleton<IConfiguration>(Configuration);
                  services.AddHostedService<ConsumerService>();
              })
              .UseConsoleLifetime()
              .Build();

            Console.WriteLine("Consumer will runn in 5 seconds");
            Thread.Sleep(5000);
            hostBuilder.Run();
        }
    }
}
