﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQPriority.Producer.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQPriority.Consumer.Services
{
    public class ConsumerService : BackgroundService
    {
        private readonly IConfiguration _configuration;
        private ConnectionFactory _factory = new ConnectionFactory();
        private IConnection conn;
        private IModel _channelOrder;

        public ConsumerService(IConfiguration configuration)
        {
            _configuration = configuration;

            var username = _configuration.GetSection("Rabbit")["Username"];
            var password = _configuration.GetSection("Rabbit")["Password"];
            var host = _configuration.GetSection("Rabbit")["Host"];
            var ip = _configuration.GetSection("Rabbit")["Ip"];

            _factory.UserName = username;
            _factory.Password = password;
            _factory.VirtualHost = host;
            _factory.HostName = ip;
            _factory.AutomaticRecoveryEnabled = true;

            conn = _factory.CreateConnection();
            _channelOrder = conn.CreateModel();
            IDictionary<String, Object> args = new Dictionary<String, Object>();
            args.Add("x-max-priority", 2);
            _channelOrder.QueueDeclare(queue: "order", durable: false, exclusive: false, autoDelete: false, arguments: args);

        }
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _channelOrder.BasicQos(0, 1, false);
            var consumer = new EventingBasicConsumer(_channelOrder);
            _channelOrder.BasicConsume("order", false, consumer);
            consumer.Received += (ch, ea) =>
            {
                try
                {
                    var content = Encoding.UTF8.GetString(ea.Body.ToArray());
                    var orderDto = JsonConvert.DeserializeObject<OrderDto>(content);

                    Console.WriteLine("Consumer: " + "id: " + orderDto.id + " actionType: " + orderDto.actionType);
                    _channelOrder.BasicAck(ea.DeliveryTag, false);
                }
                catch (Exception ex)
                {
                    _channelOrder.BasicAck(ea.DeliveryTag, false);
                }

            };


            return Task.CompletedTask;
        }
    }
}
